<?php
declare(strict_types=1);

namespace CountrySearch;

/**
 * Country Lookup Interface
 */
interface CountryLookupInterface {

	/**
	 * Lookup by name
	 *
	 * Search for $query by country name.
	 *
	 * @param string the search term
	 * @param array an array of metadata to filter api results by
	 *
	 * @return array
	 */
	public function lookupByName($query, $filters): array;

	/**
	 * Lookup by alpha code
	 *
	 * Search for $query by two or three character alpha code.
	 *
	 * @param string the search term
	 * @param array an array of metadata to filter api results by
	 *
	 * @return array
	 */
	public function lookupByAlphaCode($code, $filters): array;

}

?>