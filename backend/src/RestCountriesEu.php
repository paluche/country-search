<?php
declare(strict_types=1);

namespace CountrySearch;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;

/**
 * restcountries.eu REST API bindings
 *
 * @implements CountryLookupInterface
 */
class RestCountriesEu implements CountryLookupInterface {

	/** @var string the base component of the REST API URLs */
	const REST_BASE_URI = 'https://restcountries.eu/rest/v2/';

	/** @var string relative URL path for name lookup */
	const REST_NAME_PATH = 'name/';

	/** @var string relative URL path for alpha code lookup */
	const REST_ALPHA_CODE_PATH = 'alpha/';

	/** @var ClientInterface a Guzzle HTTP client */
	private $httpRequestClient;

	public function __construct(ClientInterface $httpRequestClient) {
		$this->httpRequestClient = new $httpRequestClient;
	}

	/**
	 * Lookup by name
	 *
	 * Search for $query by country name.
	 *
	 * @param string the search term
	 * @param array an array of metadata to filter api results by
	 *
	 * @return array
	 */
	public function lookupByName($query, $filters): array {
		return $this->lookupBy(self::REST_NAME_PATH, $query, $filters);
	}

	/**
	 * Lookup by alpha code
	 *
	 * Search for $query by two or three character alpha code.
	 *
	 * @param string the search term
	 * @param array an array of metadata to filter api results by
	 *
	 * @return array
	 */
	public function lookupByAlphaCode($code, $filters): array {
		return $this->lookupBy(self::REST_ALPHA_CODE_PATH, $code, $filters);
	}

	/**
	 * Lookup
	 *
	 * Search for $query by $path.
	 *
	 * @param string the search term
	 * @param array an array of metadata to filter api results by
	 *
	 * @return array
	 */
	private function lookupBy($path, $query, $filters): array {

		$filters = "?fields=" . implode(';', $filters);

		try {
			$response = $this->httpRequestClient->request('GET', self::REST_BASE_URI . $path . $query . $filters);
		} catch (TransferException $e) {
			if ($e->hasResponse()) {
				return [];
			}
		}

		$json = $response->getBody()->getContents();
		$result = json_decode($json);
 		return is_array($result) ? $result : array($result);

	}

}
