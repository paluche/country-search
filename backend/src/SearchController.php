<?php
declare(strict_types=1);

namespace CountrySearch;

use Psr\Http\Message\ResponseInterface;
/**
 * Search controller
 *
 * Handle country search requests.
 */
class SearchController {

	/** @var ResponseInterface a PSR-7 response interface */
	private $response;

	/** @var CountryLookupInterface */
	private $countryLookup;

	/** @var array the lengths of valid alpha codes */
	const ALPHA_CODE_LENGTHS = array(2, 3);

	/** @var integer the maximum amount of results to return */
	const RESULT_LIMIT = 50;

	/* This is restcountries.eu specific, but I'm not sure how to design it such
	   that this controller can ask for certain types of information about a
	   country without knowing how the class fetching the data refers to those
	   bits of information... */

	/** @var array the country metadata to fetch */
	const FILTERS = array(
		'name',
		'alpha2Code',
		'alpha3Code',
		'flag',
		'region',
		'subregion',
		'population',
		'languages',
	);

	public function __construct(
		ResponseInterface $response,
		CountryLookupInterface $countryLookup
	) {
		$this->response = $response;
		$this->countryLookup = $countryLookup;
	}

	/**
	 * Handle search
	 *
	 * Get the search term from the request, search for it, limit the results,
	 * send back a response.
	 *
	 * @param ServerRequest $request a PSR-7 Request interface
	 *
	 * @return ResponseInterface
	 */
	public function __invoke($request): ResponseInterface {

		$query = $request->getQueryParams()['query'];

		$results = $this->searchCountries($query);
		$results = $this->limitResultCount($results);
		$response = $this->generateResponse($results);

		return $response;

	}

	/**
	 * Generate response
	 *
	 * Generate a PSR-7 response containing JSON representing the search results
	 *
	 * @param array $results an array whose elements represent search results
	 *
	 * @return ResponseInterface
	 */
	private function generateResponse($results): ResponseInterface {

		$payload = $this->generateJSONPayload($results);

		$response = $this->response->withHeader('Content-Type', 'application/json')
						           ->withHeader('Access-Control-Allow-Origin', '*')
						           ->withHeader('Access-Control-Allow-Methods', 'GET');
		$response->getBody()->write($payload);

		return $response;

	}

	/**
	 * Generate JSON payload
	 *
	 * Determine what JSON payload to send based on search results
	 *
	 * @param array $results an array whose elements represent search results
	 *
	 * @return string
	 */
	private function generateJSONPayload($results): string {

		if (empty($results))
			return json_encode(
				["error" => ["code" => 404, "message" => "No Results"]]
			);

		return json_encode(["data" => $results]);

	}

	/**
	 * Limit result count
	 *
	 * Truncate $results to length RESULT_LIMIT.
	 *
	 * @param array $results an array whose elements represent search results
	 *
	 * @return array
	 */
	private function limitResultCount($results): array {
		return array_slice($results, 0, self::RESULT_LIMIT);
	}

	/**
	 * Search countries
	 *
	 * Use the country lookup interface to search for countries that match the
	 * given $query.
	 *
	 * @param string $query the search term
	 *
	 * @return array
	 */
	private function searchCountries($query): array {

		$results = [];

		// always search by name
		$results = array_merge($results, $this->countryLookup->lookupByName($query, self::FILTERS));

		if ($this->queryMatchesAlphaCodeLength($query))
			$results = array_merge($results, $this->countryLookup->lookupByAlphaCode($query, self::FILTERS));

		return $results;

	}

	/**
	 * Query matches alpha code length
	 *
	 * Return true if the length of $query is found in ALPHA_CODE_LENGTHS.
	 *
	 * @param string $query the search term
	 *
	 * @return bool
	 */
	private function queryMatchesAlphaCodeLength($query): bool {
		return in_array(strlen($query), self::ALPHA_CODE_LENGTHS);
	}

}
