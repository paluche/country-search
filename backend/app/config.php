<?php
declare(strict_types=1);

use function DI\autowire;
use Zend\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use CountrySearch\CountryLookupInterface;
use CountrySearch\RestCountriesEu;

return [
	ResponseInterface::class => autowire(Response::class),
	CountryLookupInterface::class => autowire(RestCountriesEu::class),
	ClientInterface::class => autowire(Client::class),
];

?>