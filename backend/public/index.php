<?php
declare(strict_types=1);

use Middlewares\FastRoute;
use FastRoute\RouteCollector;
use Relay\Relay;
use Zend\Diactoros\ServerRequestFactory;
use Middlewares\RequestHandler;
use Narrowspark\HttpEmitter\SapiEmitter;
use CountrySearch\SearchController;
use DI\ContainerBuilder;
use function FastRoute\simpleDispatcher;

require_once dirname(__DIR__) . '/vendor/autoload.php';

// dependency injection
$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions(dirname(__DIR__) . '/app/config.php');
$container = $containerBuilder->build();

// map paths to callbacks.
$dispatcher = simpleDispatcher(function (RouteCollector $r) {
	$r->get('/search', SearchController::class);
});

// Relay queues are FIFO.
$middlewareQueue = [];
// Step 1 is to map a request to a specific callback.
$middlewareQueue[] = new FastRoute($dispatcher);
// Step 2 is to execute that callback. Treating these as discrete steps gives us
// the option of sticking other middlewares in between.
$middlewareQueue[] = new RequestHandler($container);

$requestHandler = new Relay($middlewareQueue);

// generate a ServerRequest from superglobals, pass it to our Relay object for
// handling.
$response = $requestHandler->handle(ServerRequestFactory::fromGlobals());

$emitter = new SapiEmitter();
return $emitter->emit($response);