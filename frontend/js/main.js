"use strict";

var $form = $('#country-search');
var $table = $('#results');
var mytablesort = new Tablesort($table[0]);

$(document).ready( function() {

	$form.submit(submitQuery);

});

/* query the API using form input. */
function submitQuery(e) {
	e.preventDefault();
	$.ajax({
		type: "GET",
		url: $form.attr('action'),
		data: $form.serialize(),
		success: querySuccess,
		error: queryFailure,
	});
}

/* on ajax success, determine success/failure of search and update table. */
function querySuccess(response) {
	if ("error" in response) {
		alert(response.error.message);
		updateTable([]);
		updateStats([]);
	} else {
		updateTable(response.data);
		updateStats(response.data);
	}
}

function queryFailure(response) {
	alert("Whoops, AJAX failed...");
}

function updateTable(data) {
	$table.find("tbody").empty();
	data.forEach( function (country) {

		var langs = country.languages.flat();
		langs = langs.map((lang) => lang.name);

        // FIXME: XSS vulnerability
		$table.find("tbody").append(`
			<tr>
				<td><img class="flag" alt="flag" src="${country.flag}"/></td>
				<td>${country.name}</td>
				<td>${country.alpha2Code}</td>
				<td>${country.alpha3Code}</td>
				<td>${country.region}</td>
				<td>${country.subregion}</td>
				<td>${country.population}</td>
				<td>${langs.join(', ')}</td>
			</tr>
		`);

	})

}

function updateStats(data) {
	$('#total').text(data.length);

	var regions = new Set(data.map((country) => country.region));
	$('#regions').text(Array.from(regions).join(', '));

	var subregions = new Set(data.map((country) => country.subregion));
	$('#subregions').text(Array.from(subregions).join(', '));


}