This frontend code is rudimentary and not fully representative of my knowledge of best practices, but I ran out of time.

You required that the search results shoud be sorted alphabetically by name and population, which I took to mean that a user should be able to sort the results by either name or population, via clicking on the table column headers.

I went for the goal of searching by country name or alpha code via a single input, which it does, but since a search for "al" could be intended as an alpha2Code search or a partial name search, it will return results for both.

# Backend

* `cd backend`
* `composer install`
* `php -S localhost:8000 -t public/`

Backend must be at localhost:8000, unfortunately. If you must put it elsewhere, you'll have to change the form action in frontend/index.html.

# Frontend

* `cd frontend`
* `npm install`
* `browserify js/main.js -o dist/bundle.js`
* `php -S localhost:8001 -t .`

Frontend can't be accessed directly via `file:///`.
